<?php

namespace App\Http\Controllers;

use App\Story;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ProfilesController extends Controller
{
    public function show(User $user) {
        return view('profiles.show', [
            'user' => $user,
            'stories' => $user->stories()->withLikes()->paginate(15)
        ]);
    }

    public function edit(User $user) {

        $this->authorize('edit', $user);

        return view('profiles.edit', compact('user'));
    }

    public function update(User $user) {

        $attributes = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'alpha_dash', Rule::unique('users')->ignore($user),],
            'avatar' => ['file', 'dimensions:min_width=100,min_height=200',],
            'banner' => ['file'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user),],
            'description' => ['max:255'],
            'password' => ['required', 'string', 'min:8', 'max:255', 'confirmed'],
        ]);

        if (\request('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');

        }

        if (\request('banner')) {
            $attributes['banner'] = request('banner')->store('banners');
        }

        $user->update($attributes);

        return redirect($user->path())->with('info', 'Your profile has been updated successfully.');
    }


}
