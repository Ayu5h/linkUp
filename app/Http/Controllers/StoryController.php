<?php

namespace App\Http\Controllers;

use App\Story;
use Illuminate\Http\Request;

class StoryController extends Controller
{
    public function index() {

        return view('stories.index', [
            'stories' => auth()->user()->timeline()
        ]);
    }

    public function store(Request $request) {
        $request->validate([
            'body' => 'required|max:255'
            ]);

        Story::create([
            'user_id' => auth()->user()->id,
            'body' => $request->body
        ]);

        return back()->with('success', 'Your story has been posted.');
    }

    public function destroy(Story $story) {
        $story->delete();

        return redirect('stories')->with('success', 'Your story has been deleted!');

    }
}
