<?php

namespace App\Http\Controllers;

use App\Story;

class StoryLikesController extends Controller
{
    public function store(Story $story) {
        $story->like(current_user());

        return back();
    }

    public function destroy(Story $story) {
        $story->dislike(current_user());

        return back();
    }
}
