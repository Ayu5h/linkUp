<x-app>
    <header class="mb-6 relative">
        <div class="relative">
            <img src="{{ $user->banner }}"
                 class="rounded-lg mb-2"
                 alt="banner"
                 style="width: 700px;height: 230px;"/>

            <img src="{{ $user->avatar }}"
                 alt=""
                 width="150"
                 style="left: 50%;"
                 class="rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2"/>
        </div>
        <div class="flex justify-between items-center mb-6">
            <div style="max-width: 270px;">
                <h2 class="font-bold text-2xl mb-2">{{ $user->name }}</h2>
                <p class="text-sm">Joined {{ $user->created_at->diffForHumans() }}</p>
            </div>

            <div class="flex">
                @can('edit', $user)
                    <a href="{{ $user->path('edit') }}" class="rounded-full border border-gray-400 py-2 px-4 text-xs mr-2">Edit Profile</a>
                @endcan

                <x-follow-button :user="$user">
                </x-follow-button>
            </div>
        </div>

        <p class="text-sm">
            @if (!empty($user->description))
                {!! $user->description !!}
            @else
                Nothing to show yet!
            @endif

        </p>



    </header>

    @include('_timeline', [
        'stories' => $stories
    ])
</x-app>

