<x:app>

    <h2 class="mb-4 font-bold text-lg">Edit the Profile</h2>

    <form method="POST" action="{{ $user->path() }}" enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        <div class="mb-6">
            <label for="name" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Name
            </label>

            <input type="text" name="name" id="name" value="{{ $user->name }}" class="border border-gray-400 p-2 w-full" required/>

            @error('name')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="username" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Username
            </label>

            <input type="text" name="username" id="username" value="{{ $user->username }}" class="border border-gray-400 p-2 w-full" required/>

            @error('username')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="avatar" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Avatar
            </label>
            <div class="flex">
                <input type="file" name="avatar" id="avatar" value="{{ $user->avatar }}" class="border border-gray-400 p-2 w-full"/>

                <img src="{{ $user->avatar }}" width="40" alt="your avatar"/>

            </div>
            @error('avatar')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror

        </div>

        <div class="mb-6">
            <label for="banner" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Banner
            </label>
            <div class="flex">
                <input type="file" name="banner" id="banner" value="{{ $user->banner }}" class="border border-gray-400 p-2 w-full"/>

                <img src="{{ $user->banner }}" width="40" alt="your banner"/>

            </div>
            @error('banner')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror

        </div>

        <div class="mb-6">
            <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Email
            </label>

            <input type="email" name="email" id="email"  value="{{ $user->email }}" class="border border-gray-400 p-2 w-full" required/>

            @error('email')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="description" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Description
            </label>

            <textarea class="border border-gray-400 p-2 w-full" name="description" value="">{!! $user->description !!}</textarea>

            @error('description')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="password" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Password
            </label>

            <input type="password" name="password" id="password" value="" class="border border-gray-400 p-2 w-full" required/>

            @error('password')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <label for="password_confirmation" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                Confirm Password
            </label>

            <input type="password" name="password_confirmation" value="" id="password_confirmation" class="border border-gray-400 p-2 w-full" required/>

            @error('password_confirmation')
            <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
            @enderror
        </div>

        <div class="mb-6">
            <button type="submit" class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-bue-500 mr-4">Submit</button>

            <a class="bg-red-400 text-white rounded py-2 px-4 hover:bg-bue-500" href="{{ $user->path() }}">Cancel</a>
        </div>
    </form>
</x:app>
