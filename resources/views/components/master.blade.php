<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/logo-final.png')}}" type="image/x-icon"/>
    <title>LinkUp</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>

        $(document).ready(function(){
            $(".alert").delay(5000).slideUp(300);
        });

    </script>
</head>
<body>
<div id="app">
    <section class="mb-6">
        <header class="container mx-auto">
            <img  style="height: 100px; width: 140px; overflow: hidden;" src="{{ asset('images/logo-final.png')}}" alt="logo"/>
        </header>
    </section>

    {{ $slot }}



</div>

<script src="https://unpkg.com/turbolinks"></script>
</body>
</html>
