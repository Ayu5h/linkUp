<button class="bg-blue-500 hover:bg-blue-600 rounded-lg shadow px-10 text-white h-10" type="submit">{{ $slot }}</button>
