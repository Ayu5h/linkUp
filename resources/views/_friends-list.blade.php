<div class="bg-gray-200 border border-gray-300  rounded-lg py-6 py-4 px-6">
    {{--    bg-blue-100 rounded-lg p-4--}}
    <h3 class="font-bold text-xl mb-4">Following</h3>

    <ul>
            @forelse(auth()->user()->follows as $user)
                <li class="{{ $loop->last ? '' : 'mb-4' }}">
                    <div>
                        <a href="{{ url('profiles', $user->username) }}" class="flex items-center text-sm">
                            <img class="rounded-full mr-2" src="{{ $user->avatar }}" width="40" height="40" alt="User_Image"/>

                            {{ $user->name }}
                        </a>
                    </div>
                </li>
        @empty
            <li class="text-sm font-bold">No Friends Yet!</li>
        @endforelse
    </ul>
</div>

@include('flash')
