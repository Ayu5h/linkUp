<div class="border border-blue-400 rounded-lg py-6 px-8 mb-8">

    <form method="POST" action="{{ url('stories') }}">
        @csrf
        <textarea class="w-full resize-none" name="body" placeholder="What's your story today?" required></textarea>

        <hr class="my-4"/>

        <footer class="flex justify-between items-center">
            <img class="rounded-full mr-2" width="50" height="50" src="{{ auth()->user()->avatar }}" alt="User_Image"/>

            <x:button>Publish!</x:button>
        </footer>

        @error('body')
        <p class="text-sm text-red-400">{{ $message }}</p>
        @enderror
    </form>
</div>
