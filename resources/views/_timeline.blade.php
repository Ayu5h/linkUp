<div class="border border-gray-300 rounded-lg">
    @forelse($stories as $story)
        @include('_story')
    @empty
        <p class="p-4">No Stories Yet!</p>
    @endforelse

        {{ $stories->links() }}
</div>
