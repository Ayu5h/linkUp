<x-app>
    <div>
        @include('_publish-story-panel')

        @include('_timeline')
    </div>
</x-app>
