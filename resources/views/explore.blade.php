<x-app>
    <div>
        @foreach ($users as $user)
            <div class="flex items-center mb-5">
                <img href="{{ $user->path() }}" class="rounded mr-4" src="{{ $user->avatar }}" alt="{{ $user->username }}" width="60"/>

                <div>
                    <a href="{{ url('profiles', $user->username) }}">
                        <h4 class="font-bold">{{'@' . $user->username }}</h4>
                    </a>
                </div>
            </div>

        @endforeach
    </div>
    {{ $users->links() }}

</x-app>
