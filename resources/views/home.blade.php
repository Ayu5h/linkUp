<x-app>
    <div class="lg:flex lg:justify-between">

        <div class="lg:w-32 ">
            @include('_sidebar-links')
        </div>

        <div class="lg:flex-1 lg:mx-10" style="max-width: 700px">
            <div class="border border-blue-400 rounded-lg py-6 px-8 mb-8">
                @include('_publish-story-panel')
            </div>

            <div class="border border-gray-300 rounded-lg">
                @foreach($stories as $story)
                    @include('_story')
                @endforeach
            </div>
        </div>

        <div class="lg:w-1/6">
            @include('_friends-list')
        </div>

    </div>
</x-app>
