<div class="flex p-4 {{ $loop->last ? '' : 'border-b border-b-gray-400' }}">
    <div class="mr-2 flex-shrink-0">
        <a href="{{ route('profile', $story->user->username) }}">
            <img class="rounded-full mr-2" src="{{ $story->user->avatar }}" width="50" height="50" alt="User_Image"/>
        </a>
    </div>

    <div>
        <h5 class="font-bold mb-4"><a href="{{ route('profile', $story->user->username) }}">{{ $story->user->name }} </a></h5>

        <p class="text-sm mb-3">
            {{ $story->body }}
        </p>

        <x:like-buttons :story="$story" />
    </div>
</div>

{{--            @can('edit', $story->user)--}}
{{--                <form method="POST" action="{{ route('stories.destroy', $story) }}">--}}
{{--                    @csrf--}}
{{--                    @method('DELETE')--}}
{{--                    <button type="submit">--}}
{{--                        <i class="fa fa-trash text-sm"></i>--}}
{{--                    </button>--}}
{{--                </form>--}}
{{--            @endcan--}}
