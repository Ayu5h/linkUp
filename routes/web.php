<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('auth')->group(function () {
    Route::get('stories', 'StoryController@index')->name('home');
    Route::post('stories', 'StoryController@store');

    Route::post('stories/{story}/like', 'StoryLikesController@store');
    Route::delete('stories/{story}/like', 'StoryLikesController@destroy');

    Route::delete('stories/{story}', 'StoryController@destroy')->name('stories.destroy');


    Route::post('profiles/{user:username}/follow', 'FollowsController@store');
    Route::get('profiles/{user:username}/edit', 'ProfilesController@edit')->middleware('can:edit,user');
    Route::patch('profiles/{user:username}', 'ProfilesController@update')->middleware('can:edit,user');

    Route::get('explore', 'ExploreController@index')->name('explore');
});

Route::get('profiles/{user:username}', 'ProfilesController@show')->name('profile');



